#!/bin/bash
if command -v kubectl &>/dev/null; then
  echo "󱃾 $(kubectl config current-context)"
fi
